﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;

namespace OhysCrawler
{
    internal static class Helpers
    {
        internal static void SetConsoleFont(string fontName = "Lucida Console")
        {
            unsafe
            {
                var hnd = WinAPI.GetStdHandle(WinAPI.STD_OUTPUT_HANDLE);
                if (hnd != WinAPI.INVALID_HANDLE_VALUE)
                {
                    var info = new WinAPI.CONSOLE_FONT_INFO_EX();
                    info.cbSize = (uint) Marshal.SizeOf(info);
                    var newInfo = new WinAPI.CONSOLE_FONT_INFO_EX();
                    newInfo.cbSize = (uint) Marshal.SizeOf(newInfo);
                    newInfo.FontFamily = WinAPI.TMPF_TRUETYPE;
                    var ptr = new IntPtr(newInfo.FaceName);
                    Marshal.Copy(fontName.ToCharArray(), 0, ptr, fontName.Length);
                    newInfo.dwFontSize = new WinAPI.COORD(info.dwFontSize.X, info.dwFontSize.Y);
                    newInfo.FontWeight = info.FontWeight;
                    WinAPI.SetCurrentConsoleFontEx(hnd, false, ref newInfo);
                }
            }
        }

        internal static Dictionary<string, string> GetHeaders(string url)
        {
            var header = new Dictionary<string, string>();
            var request = (HttpWebRequest) WebRequest.Create(url);
            var response = (HttpWebResponse) request.GetResponse();
            foreach (string headerItem in response.Headers)
                header.Add(headerItem, response.Headers[headerItem]);
            response.Close();
            return header;
        }

        internal static void ClearCurrentConsoleLine()
        {
            var currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        internal static void WriteYellow(string message, string message1)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write(message);
            Console.ResetColor();
            Console.WriteLine(message1);
        }

        internal static bool EnsureSingleInstance()
        {
            var currentProcess = Process.GetCurrentProcess();

            var runningProcess =
            (
                from process in Process.GetProcesses()
                where process.Id != currentProcess.Id
                      && process.ProcessName.Equals(currentProcess.ProcessName, StringComparison.Ordinal)
                select process
            ).FirstOrDefault();

            if (runningProcess != null) return false;

            return true;
        }

        internal static string BytesToString(long byteCount)
        {
            string[] suf =
            {
                "B",
                "KB",
                "MB",
                "GB",
                "TB",
                "PB",
                "EB"
            };
            if (byteCount == 0)
                return "0" + suf[0];
            var bytes = Math.Abs(byteCount);
            var place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            var num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return Math.Sign(byteCount) * num + suf[place];
        }
    }
}