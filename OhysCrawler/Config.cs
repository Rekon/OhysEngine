﻿namespace OhysCrawler
{
    internal struct Config
    {
        internal static readonly bool FTP = false; //FTP Upload
        internal static readonly string FTPHost = ""; //IP Address or Domain
        internal static readonly string FTPUsername = ""; //Username;
        internal static readonly string FTPPassword = ""; //Password;
        internal static readonly int FTPPort = 21; //FTP Port
        internal static readonly string FTPDBFullPath = "/data/"; //Database Upload Directory Path (database.json, updated_on.txt)
        internal static readonly string FTPTorrentPath = "/data/torrents/"; //Torrent Upload Directory Path

        internal static readonly string TorrentDownloadInitURL = "https://example.com/data/torrents/"; //Torrent Download URL Init (URL + Filename)

        internal static bool CrawlNewTorrentOnly = false; // Crawl New Torrents Only (true) or Full Torrents (false)

        internal static bool CronJob = true;
        internal static int CronJobInterval = 10; // sec
    }
}