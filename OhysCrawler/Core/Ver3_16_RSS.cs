﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Script.Serialization;
using FluentFTP;

namespace OhysCrawler.Core
{
    internal class Ver3_16_RSS
    {
        internal static void Run(string[] args)
        {
            if (!Helpers.EnsureSingleInstance())
            {
                Environment.Exit(0);
                return;
            }

            Helpers.SetConsoleFont();
            Console.Clear();
            Console.ResetColor();
            Console.Title = "Ohys-Raws Database Updater";
            Console.WriteLine();
            Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
            Console.WriteLine(" VERSION 3.16");
            Console.WriteLine();

            MainJob();
            if (Config.CronJob)
            {
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
                    Console.WriteLine(" VERSION 3.16");
                    Console.WriteLine();
                    Helpers.WriteYellow(" [!] ",
                        "CRONJOB SET: INTERVAL TO " + Config.CronJobInterval + " SEC".ToUpper());
                    Helpers.WriteYellow(" [!] ",
                        "CORE MODE: RSS + JSON (CHECK ONLY NEW TORRENTS)");
                    Thread.Sleep(Config.CronJobInterval * 1000);
                    Console.WriteLine();
                    MainJob();

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.WaitForFullGCApproach();
                }
            }

            Console.WriteLine();
            Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
            Console.WriteLine(" VERSION 3.16");
            Console.WriteLine();

            MainJob();
        }

        private static void MainJob()
        {
            List<string> torrentlist = new List<string>();
            var NewTorrent = false;
            Helpers.WriteYellow(" [DONE] ", "READING HEADERS");
            var headers = Helpers.GetHeaders("http://torrents.ohys.net/t/");
            Helpers.WriteYellow(" [DONE] ", "FINDED X-POWERED-BY: " + headers["X-Powered-By"]);
            Helpers.WriteYellow(" [DONE] ", "WEBCLIENT INIT".ToUpper());
            Console.WriteLine();
            Helpers.WriteYellow(" [DONE] ", "SET NORMAL HEADERS");
            Helpers.WriteYellow(" [RUNNING] ", "CHECKING NEW TORRENT LIST");
            Console.WriteLine();

            var urlw = "http://torrents.ohys.net/t/json.php?dir=disk";

            if (!Directory.Exists("torrents")) Directory.CreateDirectory("torrents");

            if (!Directory.Exists("output")) Directory.CreateDirectory("output");

            using (var web = new WebClient())
            {
                web.Proxy = new WebProxy();
                web.Headers.Add("user-agent",
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
                try
                {
                    var ss = web.DownloadString(urlw);

                    var ser = new JavaScriptSerializer();
                    var records = ser.Deserialize<List<JsonTemplate.OhysRawsJson>>(ss);

                    foreach (var j in records)
                        if (!File.Exists(@"torrents\" + WebUtility.HtmlDecode(j.t)))
                        {
                            Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                            web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                @"torrents\" + WebUtility.HtmlDecode(j.t));
                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                            Helpers.ClearCurrentConsoleLine();
                            Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                            torrentlist.Add(WebUtility.HtmlDecode(j.t));
                            NewTorrent = true;
                        }
                        else
                        {
                            var sw = new FileInfo(@"torrents\" + WebUtility.HtmlDecode(j.t));
                            if (sw.Length < 5 || sw.Length == 0)
                            {
                                Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                                File.Delete(@"torrents\" + WebUtility.HtmlDecode(j.t));
                                web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                    @"torrents\" + WebUtility.HtmlDecode(j.t));
                                Console.SetCursorPosition(0, Console.CursorTop - 1);
                                Helpers.ClearCurrentConsoleLine();
                                Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                torrentlist.Add(WebUtility.HtmlDecode(j.t));
                                NewTorrent = true;
                            }
                            else
                            {
                            }
                        }
                }
                catch
                {
                }
            }

            if (NewTorrent)
            {
                Console.WriteLine();
                Helpers.WriteYellow(" [DONE] ", "NEW TORRENT DETECTED");

                Helpers.WriteYellow(" [PROCESSING] ", "OUTPUT TO JSON TYPE");
                Thread.Sleep(2000);

                DatabaseBuilder.Run();

                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Helpers.ClearCurrentConsoleLine();
                Helpers.WriteYellow(" [DONE] ", "OUTPUT TO JSON TYPE");

                if (Config.FTP)
                {
                    Helpers.WriteYellow(" [PROCESSING] ", "connect to mirror server".ToUpper());

                    var client = new FtpClient(Config.FTPHost)
                    {
                        Port = Config.FTPPort,
                        Credentials = new NetworkCredential(Config.FTPUsername, Config.FTPPassword)
                    };

                    var connect = false;
                    try
                    {
                        client.Connect();
                        connect = true;
                    }
                    catch
                    {
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        Helpers.ClearCurrentConsoleLine();
                        Helpers.WriteYellow(" [FAILED] ", "connect to mirror server".ToUpper());
                        connect = false;
                    }

                    if (connect)
                    {
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        Helpers.ClearCurrentConsoleLine();
                        Helpers.WriteYellow(" [DONE] ", "connect to mirror server".ToUpper());
                        Helpers.WriteYellow(" [PROCESSING] ", "upload / update the database".ToUpper());

                        try
                        {
                            client.RetryAttempts = 3;
                            client.UploadFile(@"output\database.json", Config.FTPDBFullPath + "database.json",
                                FtpExists.Overwrite, false, FtpVerify.Retry);
                            client.UploadFile(@"output\updated_on.txt", Config.FTPDBFullPath + "updated_on.txt",
                                FtpExists.Overwrite, false, FtpVerify.Retry);
                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                            Helpers.ClearCurrentConsoleLine();
                            Helpers.WriteYellow(" [DONE] ", "upload / update the database".ToUpper());
                        }
                        catch
                        {
                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                            Helpers.ClearCurrentConsoleLine();
                            Helpers.WriteYellow(" [FAILED] ", "upload / update the database".ToUpper());
                        }
                    }

                    client.Connect();
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine();

                    Helpers.WriteYellow(" [DONE] ", "CONNECTED VIA WEBSOCKET");
                    Console.WriteLine();
                    var infoo = new DirectoryInfo("torrents");

                    int i = 0;
                    foreach (var t in torrentlist)
                    {
                        client.UploadFile(@"torrents\" + t, Config.FTPTorrentPath + t, FtpExists.Skip);
                        i++;
                        Helpers.WriteYellow(" [PROCESSING] ", String.Format("UPLOAD THE FILES TO THE SERVER [{0}/{1}]", i, torrentlist.Count));
                    }
                }
            }
            else
            {
                Helpers.WriteYellow(" [DONE] ", "NOTHING ELSE");
            }
        }
    }
}
