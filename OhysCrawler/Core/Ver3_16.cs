﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Script.Serialization;
using FluentFTP;

namespace OhysCrawler.Core
{
    internal class Ver3_16
    {
        internal static void Run(string[] args)
        {
            if (!Helpers.EnsureSingleInstance())
            {
                Environment.Exit(0);
                return;
            }

            if (args.Length > 0)
                if (args[0] == "--Full")
                    Config.CrawlNewTorrentOnly = false;

            Helpers.SetConsoleFont();
            Console.Clear();
            Console.ResetColor();
            Console.Title = "Ohys-Raws Database Updater";
            Console.WriteLine();
            Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
            Console.WriteLine(" VERSION 3.16");
            Console.WriteLine();

            MainJob();
            if (Config.CronJob)
            {
                var i = 0;
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
                    Console.WriteLine(" VERSION 3.16");
                    Console.WriteLine();
                    Helpers.WriteYellow(" [!] ",
                        "CRONJOB SET: INTERVAL TO " + Config.CronJobInterval + " SEC".ToUpper());
                    Helpers.WriteYellow(" [!] ", "CRONJOB WAITING (TASK " + i + ")".ToUpper());

                    Thread.Sleep(Config.CronJobInterval * 1000);
                    Console.WriteLine();
                    MainJob();
                    i++;

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.WaitForFullGCApproach();
                }
            }

            Console.WriteLine();
            Console.WriteLine(" OHYS-RAWS DATABASE UPDATER");
            Console.WriteLine(" VERSION 3.16");
            Console.WriteLine();

            MainJob();
        }

        private static void MainJob()
        {
            Helpers.WriteYellow(" [DONE] ", "READING HEADERS");
            var headers = Helpers.GetHeaders("http://torrents.ohys.net/t/");
            Helpers.WriteYellow(" [DONE] ", "FINDED X-POWERED-BY: " + headers["X-Powered-By"]);
            Helpers.WriteYellow(" [DONE] ", "WEBCLIENT INIT".ToUpper());
            Console.WriteLine();
            Helpers.WriteYellow(" [DONE] ", "SET NORMAL HEADERS");
            Helpers.WriteYellow(" [RUNNING] ", "TORRENT LIST LOADING");
            Console.WriteLine();

            var urlw = "http://torrents.ohys.net/t/json.php?dir=disk";

            if (!Directory.Exists("torrents")) Directory.CreateDirectory("torrents");

            if (!Directory.Exists("output")) Directory.CreateDirectory("output");

            Helpers.WriteYellow(" [DONE] ",
                Config.CrawlNewTorrentOnly
                    ? "MODE SELECTED - NEW TORRENT ONLY MODE"
                    : "MODE SELECTED - FULL DUMP MODE");

            Console.WriteLine();
            Helpers.WriteYellow(" [RUNNING] ", "1) NEW TORRENT LIST LOADING");
            Console.WriteLine();
            Console.WriteLine();
            using (var web = new WebClient())
            {
                web.Proxy = new WebProxy();
                web.Headers.Add("user-agent",
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
                try
                {
                    var ss = web.DownloadString(urlw);

                    if (ss == "[]")
                    {
                    }
                    else
                    {
                        for (var w = 0; w < 150; w++)
                        {
                            var urls = "http://torrents.ohys.net/t/json.php?dir=disk" + "&p=" + w;

                            ss = web.DownloadString(urls);

                            if (ss == "[]")
                            {
                                w = 150;
                                break;
                            }

                            var ser = new JavaScriptSerializer();
                            var records = ser.Deserialize<List<JsonTemplate.OhysRawsJson>>(ss);

                            foreach (var j in records)
                                if (!File.Exists(@"torrents\" + WebUtility.HtmlDecode(j.t)))
                                {
                                    Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                                    web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                        @"torrents\" + WebUtility.HtmlDecode(j.t));
                                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                                    Helpers.ClearCurrentConsoleLine();
                                    Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                }
                                else
                                {
                                    var sw = new FileInfo(@"torrents\" + WebUtility.HtmlDecode(j.t));
                                    if (sw.Length < 5 || sw.Length == 0)
                                    {
                                        Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                                        File.Delete(@"torrents\" + WebUtility.HtmlDecode(j.t));
                                        web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                            @"torrents\" + WebUtility.HtmlDecode(j.t));
                                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                                        Helpers.ClearCurrentConsoleLine();
                                        Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                    }
                                    else
                                    {
                                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                                        Helpers.ClearCurrentConsoleLine();
                                        Helpers.WriteYellow(" [EXISTED] ", WebUtility.HtmlDecode(j.t));
                                    }
                                }
                        }
                    }
                }
                catch
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    Helpers.ClearCurrentConsoleLine();
                    Helpers.WriteYellow(" [FAILED] ", "METHOD FAILED");
                }
            }

            Console.WriteLine();

            if (!Config.CrawlNewTorrentOnly)
            {
                Helpers.WriteYellow(" [RUNNING] ", "2) OLD TORRENT LIST LOADING");
                Console.WriteLine();

                for (var i = 0; i < 150; i++)
                {
                    var url = "http://torrents.ohys.net/t/json.php?dir=disk" + i;

                    using (var web = new WebClient())
                    {
                        web.Proxy = new WebProxy();
                        web.Headers.Add("user-agent",
                            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
                        try
                        {
                            var ss = web.DownloadString(url);

                            if (ss == "[]")
                            {
                            }
                            else
                            {
                                for (var w = 0; w < 150; w++)
                                {
                                    var urls = "http://torrents.ohys.net/t/json.php?dir=disk" + i + "&p=" + w;

                                    ss = web.DownloadString(urls);

                                    if (ss == "[]")
                                    {
                                        w = 150;
                                        break;
                                    }

                                    var ser = new JavaScriptSerializer();
                                    var records = ser.Deserialize<List<JsonTemplate.OhysRawsJson>>(ss);

                                    foreach (var j in records)
                                        if (!File.Exists(@"torrents\" + WebUtility.HtmlDecode(j.t)))
                                        {
                                            Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                                            web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                                @"torrents\" + WebUtility.HtmlDecode(j.t));
                                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                                            Helpers.ClearCurrentConsoleLine();
                                            Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                        }
                                        else
                                        {
                                            var sw = new FileInfo(@"torrents\" + WebUtility.HtmlDecode(j.t));
                                            if (sw.Length < 5 || sw.Length == 0)
                                            {
                                                Helpers.WriteYellow(" [DOWNLOADING] ", WebUtility.HtmlDecode(j.t));
                                                File.Delete(@"torrents\" + WebUtility.HtmlDecode(j.t));
                                                web.DownloadFile("http://torrents.ohys.net/t/" + j.a,
                                                    @"torrents\" + WebUtility.HtmlDecode(j.t));
                                                Console.SetCursorPosition(0, Console.CursorTop - 1);
                                                Helpers.ClearCurrentConsoleLine();
                                                Helpers.WriteYellow(" [DOWNLOADED] ", WebUtility.HtmlDecode(j.t));
                                            }
                                            else
                                            {
                                                Console.SetCursorPosition(0, Console.CursorTop - 1);
                                                Helpers.ClearCurrentConsoleLine();
                                                Helpers.WriteYellow(" [EXISTED] ", WebUtility.HtmlDecode(j.t));
                                            }
                                        }
                                }
                            }
                        }
                        catch
                        {
                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                            Helpers.ClearCurrentConsoleLine();
                            Helpers.WriteYellow(" [FAILED] ", "METHOD FAILED");
                        }
                    }
                }

                Console.WriteLine();
            }


            Helpers.WriteYellow(" [DONE] ", "TORRENT LOADED");
            Helpers.WriteYellow(" [PROCESSING] ", "OUTPUT TO JSON TYPE");
            Thread.Sleep(2000);

            DatabaseBuilder.Run();

            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Helpers.ClearCurrentConsoleLine();
            Helpers.WriteYellow(" [DONE] ", "OUTPUT TO JSON TYPE");

            if (Config.FTP)
            {
                Helpers.WriteYellow(" [PROCESSING] ", "connect to mirror server".ToUpper());

                var client = new FtpClient(Config.FTPHost)
                {
                    Port = Config.FTPPort,
                    Credentials = new NetworkCredential(Config.FTPUsername, Config.FTPPassword)
                };

                var connect = false;
                try
                {
                    client.Connect();
                    connect = true;
                }
                catch
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    Helpers.ClearCurrentConsoleLine();
                    Helpers.WriteYellow(" [FAILED] ", "connect to mirror server".ToUpper());
                    connect = false;
                }

                if (connect)
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    Helpers.ClearCurrentConsoleLine();
                    Helpers.WriteYellow(" [DONE] ", "connect to mirror server".ToUpper());
                    Helpers.WriteYellow(" [PROCESSING] ", "upload / update the database".ToUpper());

                    try
                    {
                        client.RetryAttempts = 3;
                        client.UploadFile(@"output\database.json", Config.FTPDBFullPath + "database.json",
                            FtpExists.Overwrite, false, FtpVerify.Retry);
                        client.UploadFile(@"output\updated_on.txt", Config.FTPDBFullPath + "updated_on.txt",
                            FtpExists.Overwrite, false, FtpVerify.Retry);
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        Helpers.ClearCurrentConsoleLine();
                        Helpers.WriteYellow(" [DONE] ", "upload / update the database".ToUpper());
                    }
                    catch
                    {
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        Helpers.ClearCurrentConsoleLine();
                        Helpers.WriteYellow(" [FAILED] ", "upload / update the database".ToUpper());
                    }
                }

                client.Connect();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();

                var infoo = new DirectoryInfo("torrents");

                foreach (var t in infoo.GetFiles())
                {
                    client.UploadFile(@"torrents\" + t.Name, Config.FTPTorrentPath + t.Name, FtpExists.Skip);
                    Helpers.WriteYellow(" [DONE] ", t.Name);
                }
            }
        }
    }
}