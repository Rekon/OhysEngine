﻿using OhysCrawler.Core;

namespace OhysCrawler
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (Config.CrawlNewTorrentOnly)
                Ver3_16_RSS.Run(args);
            else
                Ver3_16.Run(args);
        }
    }
}
