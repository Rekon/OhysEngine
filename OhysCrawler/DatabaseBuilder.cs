﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using OhysCrawler.TorrentParser;

namespace OhysCrawler
{
    internal static class DatabaseBuilder
    {
        internal static void Run()
        {
            if (!Directory.Exists("torrents")) Directory.CreateDirectory("torrents");

            if (!Directory.Exists("output")) Directory.CreateDirectory("output");

            var list = new List<JsonTemplate.SaveDatabase>();
            var info = new DirectoryInfo("torrents");

            foreach (var j in info.GetFiles())
            {
                var t = new Torrent(File.ReadAllBytes(@"torrents\" + j.Name));

                var unspecified = new DateTime(t.CreationDate.Year, t.CreationDate.Month, t.CreationDate.Day,
                    t.CreationDate.Hour, t.CreationDate.Minute,
                    t.CreationDate.Second, DateTimeKind.Unspecified);

                list.Add(new JsonTemplate.SaveDatabase
                {
                    Name = j.Name.Replace(".torrent", "").Replace(".mp4", ""),
                    Hash = t.SHAHash,
                    Size = Helpers.BytesToString(t.Size),
                    Resolution = Regex.Match(j.Name, @"\d{3,4}x\d{3,4}").Value,
                    MagnetURL = t.MagnetURI,
                    TorrentURL = Config.TorrentDownloadInitURL + j.Name,
                    CreatedOn = TimeZoneInfo.ConvertTime(unspecified,
                            TimeZoneInfo.FindSystemTimeZoneById("Korea Standard Time"), TimeZoneInfo.Utc)
                        .ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US")),
                    UniqueID = t.SHAHash.ToLower(new CultureInfo("en-GB")).Substring(0, 16)
                });
            }

            var ssg = list.OrderByDescending(s =>
                DateTime.ParseExact(s.CreatedOn, "MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US")));

            var serializeObject = JsonConvert.SerializeObject(ssg);

            if (!Directory.Exists("output")) Directory.CreateDirectory("output");

            File.WriteAllBytes(@"output\database.json", Encoding.UTF8.GetBytes(serializeObject));

            File.WriteAllText(@"output\updated_on.txt",
                DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US")));
        }
    }
}