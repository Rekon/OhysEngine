﻿using System.Reflection;
using Newtonsoft.Json;

namespace OhysCrawler
{
    public class JsonTemplate
    {
        [Obfuscation(Exclude = true)]
        public class OhysRawsJson
        {
            [JsonProperty("t")] public string t { get; set; }

            [JsonProperty("a")] public string a { get; set; }
        }

        [Obfuscation(Exclude = true)]
        public class SaveDatabase
        {
            [JsonProperty("name")] public string Name { get; set; }

            [JsonProperty("hash")] public string Hash { get; set; }

            [JsonProperty("size")] public string Size { get; set; }

            [JsonProperty("resolution")] public string Resolution { get; set; }

            [JsonProperty("magnet")] public string MagnetURL { get; set; }

            [JsonProperty("torrent")] public string TorrentURL { get; set; }

            [JsonProperty("created_on")] public string CreatedOn { get; set; }

            [JsonProperty("uniqid")] public string UniqueID { get; set; }
        }
    }
}